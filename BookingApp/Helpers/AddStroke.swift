//
//  AddStroke.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct AddStroke: ViewModifier {
    var cornerRadius: CGFloat
    var strokeColor: Color
    var lineWidth: CGFloat
    func body(content: Content) -> some View {
        content
            .background(RoundedRectangle(cornerRadius: cornerRadius)
                .stroke(strokeColor, lineWidth: lineWidth))
    }
}

extension View {
    func addStroke(cornerRadius: CGFloat, strokeColor: Color, lineWidth: CGFloat)-> some View {
        modifier(AddStroke(cornerRadius: cornerRadius, strokeColor: strokeColor, lineWidth: lineWidth))
    }
}
