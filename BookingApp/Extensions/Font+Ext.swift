//
//  Font+Ext.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

extension Font {
    static func urbanistFont(size: CGFloat, weight: Font.Weight) -> Font {
        return Font.custom("Urbanist", size: size)
            .weight(weight)
    }
}

