//
//  BookingAppApp.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

@main
struct BookingAppApp: App {
    var body: some Scene {
        WindowGroup {
            BookingTabView()
        }
    }
}
