//
//  TextFieldView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct TextFieldView: View {
    @Binding var text: String
    var keyboardType: UIKeyboardType
    var iconName: String?
    var rightIconName: String?
    var placeholder: String
    var isSecure: Bool = false
    
    var body: some View {
        VStack {
            Rectangle()
                .foregroundColor(.clear)
                .frame(width: 328, height: 53)
                .background(Color.grayText)
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                .overlay {
                    HStack {
                        if let icon = iconName {
                            Image(icon)
                        }
                        if isSecure {
                            SecureField(placeholder, text: $text)
                        } else {
                            TextField(placeholder, text: $text)
                                .keyboardType(keyboardType)
                        }
                        if let rightIcon = rightIconName {
                            Image(rightIcon)
                        }
                    }
                    .padding(.horizontal, 16)
                }
        }
    }
}

#Preview {
    TextFieldView(text: .constant(""), keyboardType: .emailAddress, iconName: "email",rightIconName: "email", placeholder: "Email")
}
