//
//  NextButton.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct BookingButton: View {
    var title: String
    var width: CGFloat = 320
    var height: CGFloat = 55
    
    
    var body: some View {
        Text(title)
            .foregroundColor(.white)
            .frame(width: width, height: height)
            .background(Color.brandPrimary)
            .cornerRadius(27.5)
            .shadow(radius: 5, y: 3)
    }
}

#Preview {
    BookingButton(title: "Next")
}
