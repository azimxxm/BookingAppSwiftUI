//
//  SkipButton.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct SkipButton: View {
    var title: String
    var width: CGFloat
    var height: CGFloat
    
    init(title: String, width: CGFloat = 320, height: CGFloat = 55) {
        self.title = title
        self.width = width
        self.height = height
    }
    
    
    var body: some View {
        Text(title)
            .foregroundColor(Color.brandPrimary)
            .frame(width: width, height: height)
            .background(Color.buttonBGGreen)
            .cornerRadius(27.5)
    }
}

#Preview {
    SkipButton(title: "Skip")
}
