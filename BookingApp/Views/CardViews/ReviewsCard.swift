//
//  ReviewsCard.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 15/10/23.
//

import SwiftUI

struct ReviewsCard: View {
    var body: some View {
        VStack {
            HStack {
                Image("person")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .clipped()
                    .clipShape(Circle())
                
                VStack(alignment: .leading) {
                    Text("Ivande Othawa")
                        .font(.urbanistFont(size: 20, weight: .bold))
                        .kerning(0.2)
                    Text("Jan 20, 2025")
                        .font(.urbanistFont(size: 14, weight: .regular))
                }
                
                HStack {
                    Image("star.white")
                        .resizable()
                        .frame(width: 24, height: 24)
                    Text("5.6")
                        
                }
                .frame(height: 10)
                .padding()
                .font(.urbanistFont(size: 14, weight: .bold))
                .background(Color.brandPrimary)
                .clipShape(RoundedRectangle(cornerRadius: 16))
                .foregroundStyle(Color.white)
            }
            Text("Very nice and comfortable hotel, thank you for accompanying my vacation!")
                .font(.urbanistFont(size: 14, weight: .regular))
            .kerning(0.2)
            .foregroundColor(.black)
        }
        .padding(20)
        .addStroke(cornerRadius: 20, strokeColor: Color.textBody, lineWidth: 0.5)
    }
}

#Preview {
    ReviewsCard()
}
