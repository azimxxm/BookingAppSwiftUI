//
//  HomeCardView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 14/10/23.
//

import SwiftUI


struct HomeCard: View {
    let imageName: String
    let rating: Double
    let hoteName: String
    let address: String
    let price: Double
    let status: String
    var body: some View {
        ZStack {
            Image(imageName)
                .resizable()
                .aspectRatio(contentMode: .fill)
            Rectangle()
                .foregroundColor(.clear)
                .frame(width: 68, height: 30)
                .background(Color.brandPrimary)
                .cornerRadius(20)
                .overlay {
                    HStack {
                        Image("star.white")
                            .resizable()
                            .frame(width: 20, height: 20)
                        Text(String(format: "%.2f", rating))
                            .font(.urbanistFont(size: 16, weight: .semibold))
                            .foregroundStyle(Color.white)
                    }
                }
                .offset(x: 80, y: -120)
            HStack(spacing: 16) {
                VStack(alignment: .leading, spacing: 6) {
                    
                    Text(hoteName)
                        .font(.urbanistFont(size: 20, weight: .bold))
                    Text(address)
                        .font(.urbanistFont(size: 14, weight: .medium))
                    Text("$\(String(format: "%.2f", price)) /\(status)")
                        .font(.urbanistFont(size: 18, weight: .bold))
                }
                Image("bookmark")
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 32, height: 28)
            }
            .kerning(0.2)
            .foregroundColor(.white)
            .offset(x: 10, y: 100)
            
        }
        .frame(width: 266, height: 335)
        .clipped()
        .cornerRadius(41)
        
    }
}

#Preview {
    HomeCard(imageName: "house", rating: 2.0, hoteName: "ATECA Hotel Suites", address: "44A Kohinur St, Tashkent", price: 250, status: "night")
}
