//
//  HotelGridCard.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 15/10/23.
//

import SwiftUI

struct HotelGridCard: View {
    let imageName: String
    let hotelName: String
    let address: String
    let rating: Double
    let price: Int
    let status: String
    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            Image(imageName)
                .resizable()
                .frame(width: 140, height: 130)
                .clipped()
                .clipShape(RoundedRectangle(cornerRadius: 10))
            Text(hotelName)
                .font(.urbanistFont(size: 14, weight: .semibold))
                .kerning(0.2)
                .foregroundStyle(Color.textTitle)
            HStack {
                Image("star.white")
                    .resizable()
                    .renderingMode(.template)
                    .foregroundStyle(Color.orange)
                    .frame(width: 18, height: 18)
                Text(String(format: "%.2f", rating))
                    .font(.urbanistFont(size: 14, weight: .bold))
                    .foregroundStyle(Color.brandPrimary)
                Text(address)
                    .font(.urbanistFont(size: 14, weight: .bold))
                    .foregroundStyle(Color.textBody)
            }
            
            HStack {
                Text("\(price)$")
                    .font(.urbanistFont(size: 16, weight: .bold))
                    .foregroundStyle(Color.brandPrimary)
                
                Text(status)
                    .font(.urbanistFont(size: 16, weight: .bold))
                    .foregroundStyle(Color.textBody)
                
                Image("bookmark")
                    .resizable()
                    .renderingMode(.template)
                    .foregroundStyle(Color.brandPrimary)
                    .frame(width: 22, height: 24)
                    .aspectRatio(contentMode: .fit)
            }
        }
        .padding()
        .addStroke(cornerRadius: 20, strokeColor: Color.textBody, lineWidth: 0.4)
    }
}

#Preview {
    HotelGridCard(imageName: "house1", hotelName: "Radisson Blu Hotel", address: "Lagos, Nigeria", rating: 5.0, price: 250, status: "night")
}
