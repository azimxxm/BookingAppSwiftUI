//
//  HotelListCard.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 14/10/23.
//

import SwiftUI

struct HotelListCard: View {
    let imageName: String
    let hotelName: String
    let address: String
    let rating: Double
    let reviews: Int
    let price: Int
    let status: String
    var body: some View {
        HStack {
            Image(imageName)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 91, height: 91)
                .clipped()
                .clipShape(RoundedRectangle(cornerRadius: 10))
            
            VStack(alignment: .leading, spacing: 6) {
                Text(hotelName)
                    .font(.urbanistFont(size: 18, weight: .bold))
            
                Text(address)
                    .font(.urbanistFont(size: 14, weight: .regular))
                .kerning(0.2)
                
                HStack {
                    Image("star.white")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundStyle(Color.orange)
                        .frame(width: 20, height: 20)
                    Text(String(format: "%.2f", rating))
                        .font(.urbanistFont(size: 16, weight: .bold))
                        .foregroundStyle(Color.brandPrimary)
                    Text("\(reviews) reviews")
                        .font(.urbanistFont(size: 16, weight: .bold))
                        .foregroundStyle(Color.textBody)
                }
            }
            VStack {
                Text(String(price) + " $")
                    .font(.urbanistFont(size: 18, weight: .bold))
                    .foregroundStyle(Color.brandPrimary)
                
                Text(status)
                    .font(.urbanistFont(size: 18, weight: .bold))
                    .foregroundStyle(Color.textBody)
                
                Image("bookmark")
                    .resizable()
                    .renderingMode(.template)
                    .foregroundStyle(Color.brandPrimary)
                    .frame(width: 25, height: 26)
                    .aspectRatio(contentMode: .fit)
            }
            
        }
        .padding()
        .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 0.4)
    }
}

#Preview {
    HotelListCard(imageName: "house1", hotelName: "Intercontinental Hotel", address: "Lagos, Nigeria", rating: 4.5, reviews: 23234, price: 250, status: "night")
}
