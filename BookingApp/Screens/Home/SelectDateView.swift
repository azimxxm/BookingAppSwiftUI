//
//  SelectDateView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 15/10/23.
//

import SwiftUI

struct SelectDateView: View {
    @State private var selectDate: Date = .now
    @State private var firstSelection: Bool = false
    @State private var personCount: Int = 0
    let today = Date()
    let futureDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())!
    var body: some View {
        NavigationStack {
            ScrollView {
                
                DatePicker("Select a Date", selection: $selectDate, in: today...futureDate)
                    .labelsHidden()
                    .datePickerStyle(.graphical)
                
                HStack(alignment: .center) {
                    VStack {
                        Text("Check in")
                            .font(.urbanistFont(size: 18, weight: .bold))
                            .kerning(0.2)
                            .multilineTextAlignment(.center)
                        HStack {
                            Text("Dec 13")
                            Image("calendar")
                        }
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.grayText)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                    }
                    Image("play.black")
                        .resizable()
                        .frame(width: 30, height: 30)
                    VStack {
                        Text("Check out")
                            .font(.urbanistFont(size: 18, weight: .bold))
                            .kerning(0.2)
                            .multilineTextAlignment(.center)
                        HStack {
                            Text("Dec 15")
                            Image("calendar")
                        }
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.grayText)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                    }
                }
                .padding()
                
                HStack {
                    Spacer()
                    Rectangle()
                        .foregroundColor(.clear)
                        .frame(width: 44, height: 44)
                        .background(Color.buttonBGGreen)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .overlay {
                            ZStack {
                                Button(action: { personCount += 1 }, label: {
                                    Text("+")
                                        .font(.urbanistFont(size: 20, weight: .bold))
                                })
                            }
                        }
                    Spacer()
                    Text("\(personCount)")
                        .font(.largeTitle)
                    Spacer()
                    Rectangle()
                        .foregroundColor(.clear)
                        .frame(width: 44, height: 44)
                        .background(Color.buttonBGGreen)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .overlay {
                            ZStack {
                                Button(action: { personCount -= 1 }, label: {
                                    Text("-")
                                        .font(.urbanistFont(size: 20, weight: .bold))
                                })
                            }
                        }
                    Spacer()
                }
                .padding(.bottom, 20)
                Text("Total: $525")
                    .font(.urbanistFont(size: 24, weight: .bold))
                    .kerning(0.2)
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 20)
                BookingButton(title: "Continue")
                Spacer()
            }
            .navigationTitle("Select Date")
        }
    }
}

#Preview {
    SelectDateView()
}
