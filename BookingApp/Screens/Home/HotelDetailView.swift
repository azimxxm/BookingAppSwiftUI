//
//  HotelDetailView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 15/10/23.
//

import SwiftUI

struct HotelDetailView: View {
    var body: some View {
        NavigationStack {
            ScrollView {
                Image("hotelRoom")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                VStack(spacing: 16) {
                    Text("Presidential Hotel")
                        .font(.urbanistFont(size: 31, weight: .bold))
                    .kerning(0.2)
                    .multilineTextAlignment(.center)
                    HStack {
                        Image("location")
                            .resizable()
                            .frame(width: 15, height: 15)
                        Text("12 Eze Adele Road Rumuomasi Lagos Nigeria")
                            .font(.urbanistFont(size: 14, weight: .medium))
                            .foregroundStyle(Color.textTitle)
                    }
                    .kerning(0.2)
                    .multilineTextAlignment(.center)
                    
                    Divider()
                        .padding(16)
                    HStack {
                        Text("Gallery Photos")
                            .font(.urbanistFont(size: 20, weight: .bold))
                            .kerning(0.2)
                        Spacer()
                        Button(action: {}, label: {
                            Text("See All")
                                .font(.urbanistFont(size: 19, weight: .bold))
                            .kerning(0.2)
                            .foregroundStyle(Color.brandPrimary)
                        })
                    }
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(0..<20) {_ in
                                Image("hotelRoom")
                                    .resizable()
                                    .frame(width: 135, height: 107)
                                    .aspectRatio(contentMode: .fill)
                                    .clipped()
                                    .clipShape(RoundedRectangle(cornerRadius: 10))
                            }
                        }
                    }
                    .scrollIndicators(.hidden)
                    
                    HStack {
                        Text("Details")
                            .font(.urbanistFont(size: 24, weight: .bold))
                        .kerning(0.2)
                        Spacer()
                    }
                    
                    HStack(spacing: 40) {
                        VStack {
                            Image("hotels")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .aspectRatio(contentMode: .fit)
                            Text("Hotels")
                                .font(.urbanistFont(size: 14, weight: .bold))
                            .kerning(0.2)
                        }
                        
                        VStack {
                            Image("badrooms")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .aspectRatio(contentMode: .fit)
                            Text("Hotels")
                                .font(.urbanistFont(size: 14, weight: .bold))
                            .kerning(0.2)
                        }
                        VStack {
                            Image("barthrooms")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .aspectRatio(contentMode: .fit)
                            Text("Hotels")
                                .font(.urbanistFont(size: 14, weight: .bold))
                            .kerning(0.2)
                        }
                        VStack {
                            Image("sqft")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .aspectRatio(contentMode: .fit)
                            Text("Hotels")
                                .font(.urbanistFont(size: 14, weight: .bold))
                            .kerning(0.2)
                        }
                    }
                    HStack {
                        Text("Description")
                            .font(.urbanistFont(size: 24, weight: .bold))
                        .kerning(0.2)
                        Spacer()
                    }

                    
                    Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt aliqua. Read more...")
                    .font(Font.custom("Urbanist", size: 14))
                    .foregroundStyle(Color.textBody)
                    .kerning(0.2)
                    
                    HStack {
                        Text("Facilities")
                            .font(.urbanistFont(size: 24, weight: .bold))
                        .kerning(0.2)
                        Spacer()
                    }
                    
                    VStack(alignment: .center, spacing: 24) {
                        HStack(alignment: .center, spacing: 30) {
                            VStack {
                                Image("bx_swim")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Swimming Pool")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("fa-solid_wifi")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("WiFi")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("ic_outline-restaurant-menu")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Restaurant")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("mdi_car-brake-parking")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Parking")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                        }
                        HStack(alignment: .center, spacing: 30) {
                            VStack {
                                Image("ic_baseline-meeting-room")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Meeting Room")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("ic_outline-elevator")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Elevator")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("fitness-center")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("Fitness Center")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                            VStack {
                                Image("ri_24-hours-line")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .aspectRatio(contentMode: .fit)
                                Text("24-hours Open")
                                    .font(.urbanistFont(size: 14, weight: .bold))
                                .kerning(0.2)
                            }
                        }
                    }
                    HStack {
                        Text("Location")
                            .font(.urbanistFont(size: 24, weight: .bold))
                        .kerning(0.2)
                        Spacer()
                    }
                    RoundedRectangle(cornerRadius: 16)
                        .fill(Color.orange.opacity(0.25))
                    
                    HStack {
                        Text("Review")
                            .font(.urbanistFont(size: 20, weight: .bold))
                        .kerning(0.2)
                        HStack {
                            Image("star.white")
                                .resizable()
                                .renderingMode(.template)
                                .foregroundStyle(Color.orange)
                                .frame(width: 20, height: 20)
                            Text(String(format: "%.2f", 5.6))
                                .font(.urbanistFont(size: 16, weight: .bold))
                                .foregroundStyle(Color.brandPrimary)
                            Text("\(23523) reviews")
                                .font(.urbanistFont(size: 16, weight: .bold))
                                .foregroundStyle(Color.textBody)
                            Spacer()
                            Button(action: {}) {
                                Text("See all")
                                .foregroundStyle(Color.brandPrimary)
                                .font(.urbanistFont(size: 18, weight: .semibold))
                            }
                        }
                        Spacer()
                    }
                    ReviewsCard()
                    ReviewsCard()
                    ReviewsCard()
                    Divider()
                    HStack {
                        Text("$205 /night")
                            .font(.urbanistFont(size: 22, weight: .bold))
                        .kerning(0.2)
                        BookingButton(title: "Book Now!", width: 200)
                    }
                    Spacer()
                }
                .padding(.horizontal, 16)
            }
            .ignoresSafeArea()
        }
    }
}

#Preview {
    HotelDetailView()
}
