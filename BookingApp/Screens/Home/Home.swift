//
//  Home.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 14/10/23.
//

import SwiftUI

struct Home: View {
    @State private var searchText = ""
    var body: some View {
        NavigationStack {
            
            HStack(spacing: 20) {
                Image("logo.green.line")
                    .resizable()
                    .frame(width: 48, height: 48)
                Text("Home")
                    .font(.urbanistFont(size: 24, weight: .bold))
                    .kerning(0.2)
                Spacer()
                Button(action: {}, label: {
                    Image("notification")
                })
                Button(action: {}, label: {
                    Image("bookmark")
                })
            }
            Spacer()
            
            
            HStack {
                Text("Hello, Sarvar")
                    .font(.urbanistFont(size: 32, weight: .bold))
                    .kerning(0.2)
                Spacer()
            }
            
            TextFieldView(text: $searchText, keyboardType: .default, iconName: "search", placeholder: "Search")
                .padding(.bottom, 20)
            ScrollView(.horizontal) {
                HStack(spacing: 20) {
                    Text("Recommended")
                        .foregroundStyle(Color.white)
                        .font(.urbanistFont(size: 20, weight: .bold))
                        .frame(height: 32)
                        .padding()
                        .background(Color.brandPrimary)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                    
                    Text("Popular")
                        .foregroundStyle(Color.brandPrimary)
                        .font(.urbanistFont(size: 20, weight: .bold))
                        .frame(height: 32)
                        .padding()
                        .addStroke(cornerRadius: 10, strokeColor: Color.brandPrimary, lineWidth: 2)
                    
                    Text("Other")
                        .foregroundStyle(Color.brandPrimary)
                        .font(.urbanistFont(size: 20, weight: .bold))
                        .frame(height: 32)
                        .padding()
                        .addStroke(cornerRadius: 10, strokeColor: Color.brandPrimary, lineWidth: 2)
                }
            }
            .scrollIndicators(.hidden)
            Spacer()
            
            ScrollView(.horizontal) {
                HStack(spacing: 24) {
                    HomeCard(imageName: "house", rating: 2.0, hoteName: "ATECA Hotel Suites", address: "44A Kohinur St, Tashkent", price: 250, status: "night")
                    HomeCard(imageName: "house1", rating: 5.0, hoteName: "ATECA Hotel Suites", address: "44A Kohinur St, Tashkent", price: 250, status: "night")
                    HomeCard(imageName: "house", rating: 7.0, hoteName: "ATECA Hotel Suites", address: "44A Kohinur St, Tashkent", price: 250, status: "night")
                }
            }
            .scrollIndicators(.hidden)
            HStack {
                Text("Recently Booked")
                    .font(.urbanistFont(size: 18, weight: .bold))
                .kerning(0.2)
                .foregroundColor(.black)
                Spacer()
                Button(action: {}, label: {
                    NavigationLink(destination: HotelRoomsView()) {
                        Text("See all")
                        .foregroundStyle(Color.brandPrimary)
                        .font(.urbanistFont(size: 18, weight: .semibold))
                    }
                })
            }
            .padding()
        }
        .padding(20)
        
    }
}

#Preview {
    Home()
}
