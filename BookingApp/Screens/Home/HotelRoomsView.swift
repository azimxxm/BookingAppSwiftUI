//
//  HotelRoomsList.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 14/10/23.
//

import SwiftUI

struct HotelRoomsView: View {
    @State private var selectGridStyle: Bool = true
    let column = [
        GridItem(.flexible(minimum: 134, maximum: 170), spacing: 20),
        GridItem(.flexible(minimum: 134, maximum: 170), spacing: 20),
    ]
    var body: some View {
        NavigationStack {
            ScrollView {
                if selectGridStyle {
                    LazyVGrid(columns: column, spacing: 16, content: {
                        ForEach(1...50, id: \.self) { count in
                            NavigationLink(destination: HotelDetailView()) {
                                HotelGridCard(imageName: "house1", hotelName: "Radisson Blu Hotel", address: "Lagos, Nigeria", rating: 5.0, price: count, status: "night")
                            }
                        }
                    })
                    .padding(.horizontal, 6)
                    .padding(.vertical, 4)
                } else {
                    LazyVStack(spacing: 16, content: {
                        ForEach(1...50, id: \.self) { count in
                            NavigationLink(destination: HotelDetailView()) {
                                HotelListCard(imageName: "house1", hotelName: "Intercontinental Hotel", address: "Lagos, Nigeria", rating: 4.5, reviews: 23234, price: count, status: "night")
                                    .foregroundStyle(Color.textTitle)
                            }
                        }
                    })
                }
            }
            .navigationTitle("ATECA Hotel Rooms")
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button(action: { selectGridStyle.toggle() }, label: {
                        Image(selectGridStyle ?  "list" : "grid")
                            .renderingMode(.template)
                            .foregroundStyle(selectGridStyle ? Color.brandPrimary : Color.textBody)
                    })
                }
            }
        }
    }
}

#Preview {
    HotelRoomsView()
}
