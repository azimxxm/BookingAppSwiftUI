//
//  OTPTextField.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 10/10/23.
//

import SwiftUI

struct OTPTextField: View {
    let numberOfFields: Int
    @State var enterValue: [String]
    @State var oldValue = ""
    @FocusState private var fieldFocus: Int?
    
    init(numberOfFields: Int) {
        self.numberOfFields = numberOfFields
        self.enterValue = Array(repeating: "", count: numberOfFields)
    }
    
    var body: some View {
        HStack {
            ForEach(0..<numberOfFields, id: \.self) { index in
                TextField("", text: $enterValue[index]) { editing in
                    if editing {
                        oldValue = enterValue[index]
                    }
                }
                .keyboardType(.numberPad)
                .frame(width: 64, height: 64)
                .background(Color.gray.opacity(0.1))
                .clipShape(RoundedRectangle(cornerRadius: 5))
                .addStroke(cornerRadius: 10, strokeColor: Color.brandPrimary, lineWidth: 1)
                .multilineTextAlignment(.center)
                .focused($fieldFocus, equals: index)
                .tag(index)
                .onChange(of: enterValue[index], perform: { newValue in
                    checkFields(index: index, newValue: newValue)
                })
            }
        }
    }
    
    private func checkFields(index: Int, newValue: String) {
        if enterValue[index].count > 1 {
            let currentValue = Array(enterValue[index])
            if currentValue[0] == Character(oldValue) {
                enterValue[index] = String(enterValue[index].suffix(1))
            } else {
                enterValue[index] = String(enterValue[index].prefix(1))
            }
        }
        if !newValue.isEmpty {
            if index == numberOfFields - 1 {
                fieldFocus = nil
            } else {
                fieldFocus = (fieldFocus ?? 0) + 1
            }
        } else {
            fieldFocus = (fieldFocus ?? 0) - 1
        }
    }
}



#Preview {
    OTPTextField(numberOfFields: 4)
}

