//
//  ForgotPassword.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 10/10/23.
//

import SwiftUI

struct ForgotPassword: View {
    var body: some View {
        NavigationStack {
            VStack(spacing: 16) {
                Image("lockForgot")
                    .resizable()
                    .frame(width: 250, height: 250)
                    .aspectRatio(contentMode: .fit)
                    .padding(16)
                
                Text("Select which contact details should we use to reset your password")
                    .font(.urbanistFont(size: 15, weight: .medium))
                    .kerning(0.2)
                VStack(spacing: 16) {
                    InfoCardForgotPassword(imageName: "symbols_sms", message: "via SMS: \n +234111******99")
                    InfoCardForgotPassword(imageName: "ic_email", message: "via Email: \n kez***9@your domain.com")
                }
                NavigationLink {
                    VerifyOTPCode()
                } label: {
                    BookingButton(title: "Continue")
                }

            }
            .background(Color.bg)
            .navigationTitle("Forgot password")
        }
    }
}

#Preview {
    ForgotPassword()
}


struct InfoCardForgotPassword: View {
    var imageName: String
    var message: String
    
    var body: some View {
        HStack(spacing: 20) {
            ZStack {
                Circle()
                    .fill(Color.green.opacity(0.25))
                    .frame(width: 80, height: 80)
                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 33, height: 33)

            }
            VStack(alignment: .leading, spacing: 8) {
                Text(message)
            }
            Spacer()
        }
        .padding(20)
        .frame(maxWidth: .infinity)
        .addStroke(cornerRadius: 10, strokeColor: Color.brandPrimary, lineWidth: 1)
        .padding(.horizontal, 16)
    }
}
