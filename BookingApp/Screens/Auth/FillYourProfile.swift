//
//  FillYourProfile.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 10/10/23.
//

import SwiftUI

struct FillYourProfile: View {
    @State private var fullName = ""
    @State private var nickName = ""
    @State private var birthDate = ""
    @State private var email = ""
    @State private var genderSelected = "Male"
    var genderData = ["Male", "Fmale", "Other"]
    
    var body: some View {
        NavigationStack {
            VStack(spacing: 24) {
                TextFieldView(text: $fullName, keyboardType: .default, iconName: nil, placeholder: "Full Name")
                TextFieldView(text: $nickName, keyboardType: .default, iconName: nil, placeholder: "Nickname")
                TextFieldView(text: $birthDate, keyboardType: .default, iconName: nil, rightIconName: "calendar", placeholder: "Date of Birth")
                TextFieldView(text: $email, keyboardType: .default, iconName: nil, rightIconName: "email", placeholder: "Email")
                    .navigationTitle("Fill Your Profile")
                Rectangle()
                    .foregroundColor(.clear)
                    .frame(width: 328, height: 53)
                    .background(Color.grayText)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                    .overlay {
                       
                        HStack {
                            Spacer()
                            Picker("Select gender", selection: $genderSelected) {
                                ForEach(genderData, id: \.self) {
                                    Text($0)
                                }
                        }
                            .pickerStyle(.navigationLink)
                            .foregroundStyle(Color.textTitle)
                        }
                        .padding()
                }
                    .padding(.bottom, 32)
                Button(action: {  }, label: {
                    BookingButton(title: "Save")
                })
            }
        }
    }
}

#Preview {
    FillYourProfile()
}
