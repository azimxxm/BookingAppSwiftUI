//
//  LetsYouInSection.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct LetsYouInSection: View {
    
    var body: some View {
        NavigationStack {
            VStack(spacing: 40) {
                Text("Let’s you in")
                    .font(.urbanistFont(size: 45, weight: .bold))
                    .kerning(0.2)
                    .foregroundColor(.black)
                VStack(spacing: 23) {
                    Group {
                        Button(action: {}, label: {
                            Label("Continue with Facebook", image: "facebook")
                        })
                        
                        Button(action: {}, label: {
                            Label("Continue with Google", image: "google")
                        })
        
                        Button(action: {}, label: {
                            Label("Continue with Apple", image: "apple")
                        })
                    }
                    .frame(maxWidth: .infinity)
                    .frame(height: 54)
                    .foregroundStyle(Color.textBody)
                    .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                    .padding(.horizontal, 31)
                }
                HStack {
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.textBody)
                    Text("OR")
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.textBody)
                }
                .padding(.horizontal, 31)
                NavigationLink {
                    LoginView()
                    
                } label: {
                    BookingButton(title: "Sign in with password")
                }

                HStack {
                    Text("Don’t have an account?")
                    NavigationLink {
                        FillYourProfile()
                    } label: {
                        Text("Sign up")
                            .foregroundStyle(Color.brandPrimary)
                            .bold()
                    }

                }
            }
        }
        .background(Color.bg)
    }
}

#Preview {
    LetsYouInSection()
}
