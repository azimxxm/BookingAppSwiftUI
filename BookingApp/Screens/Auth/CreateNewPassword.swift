//
//  CreateNewPassword.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 10/10/23.
//

import SwiftUI

struct CreateNewPassword: View {
    @State private var password = ""
    @State private var newPassword = ""
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading, spacing: 16) {
                Text("Create Your New Password")
                    .font(.urbanistFont(size: 15, weight: .semibold))
                    .kerning(0.2)
                VStack(spacing: 32) {
                    
                    TextFieldView(text: $password, keyboardType: .default, iconName: "lock", placeholder: "New Password")
                    TextFieldView(text: $password, keyboardType: .default, iconName: "lock", placeholder: "New Password")
                    
                    HStack {
                        Button(action: {}, label: {
                            Rectangle()
                                .fill(.clear)
                                .addStroke(cornerRadius: 4, strokeColor: Color.brandPrimary, lineWidth: 2)
                                .frame(width: 20, height: 20)
                            Text("Remember me")
                                .font(.urbanistFont(size: 16, weight: .medium))
                            .kerning(0.2)
                            .multilineTextAlignment(.center)
                            .foregroundStyle(Color.textTitle)
                        })
                    }
                    
                    Button(action: {}, label: {
                        BookingButton(title: "Continue")
                    })
                }
            }
            .navigationTitle("Create New Password")
        }
    }
}

#Preview {
    CreateNewPassword()
}
