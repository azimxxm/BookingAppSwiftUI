//
//  CreateAccountView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    
    var body: some View {
        NavigationStack {
            VStack(spacing: 40) {
                Text("Create your Account")
                    .font(.urbanistFont(size: 32, weight: .bold))
                    .kerning(0.2)
                    .foregroundColor(.black)
                
                VStack(spacing: 16) {
                    TextFieldView(text: $email, keyboardType: .emailAddress, iconName: "email", placeholder: "Email")
                        .padding(.bottom, 16)
                    
                    TextFieldView(text: $password, keyboardType: .default, iconName: "lock", placeholder: "Password", isSecure: true)
                }
                
                HStack {
                    Button(action: {}, label: {
                        Rectangle()
                            .fill(.clear)
                            .addStroke(cornerRadius: 4, strokeColor: Color.brandPrimary, lineWidth: 2)
                            .frame(width: 20, height: 20)
                        Text("Remember me")
                            .font(.urbanistFont(size: 16, weight: .medium))
                        .kerning(0.2)
                        .multilineTextAlignment(.center)
                        .foregroundStyle(Color.textTitle)
                    })
                }
                
                Button(action: { }, label: {
                    BookingButton(title: "Sign in")
                })
                NavigationLink {
                    ForgotPassword()
                } label: {
                    Text("Forgot the password?")
                        .foregroundStyle(Color.brandPrimary)
                        .font(.urbanistFont(size: 16, weight: .semibold))
                }

                
                HStack {
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.textBody)
                    Text("continue with")
                        .multilineTextAlignment(.center)
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.textBody)
                }
                
                HStack {
                    Group {
                        Image("facebook")
                        Image("google")
                        Image("apple")
                    }
                    .padding(40)
                    .frame(height: 70)
                    .addStroke(cornerRadius: 10, strokeColor: Color.textBody, lineWidth: 1)
                }
                
                HStack {
                    Text("Don’t have an account? ")
                    NavigationLink {
                        FillYourProfile()
                    } label: {
                        Text("Sign up")
                            .foregroundStyle(Color.brandPrimary)
                            .bold()
                    }
                }
            }
            .padding()
        }
    }
}

#Preview {
    LoginView()
}
