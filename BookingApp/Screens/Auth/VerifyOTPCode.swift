//
//  VerifyOTPCode.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 10/10/23.
//

import SwiftUI

struct VerifyOTPCode: View {
    var body: some View {
        NavigationStack {
            VStack(spacing: 48) {
                Image("securty")
                    .resizable()
                    .frame(width: 250, height: 250)
                    .aspectRatio(contentMode: .fit)
                    .padding(16)
                
                Text("Code has been sent to +234111******99")
                    .font(.urbanistFont(size: 15, weight: .medium))
                    .kerning(0.2)
                
                OTPTextField(numberOfFields: 4)
                
                Text("Resend code in 52 s")
                    .font(.urbanistFont(size: 15, weight: .medium))
                    .kerning(0.2)
                
                NavigationLink {
                    CreateNewPassword()
                } label: {
                    BookingButton(title: "Verify")
                }
                
            }
            .navigationTitle("Forgot password")
        }
    }
}

#Preview {
    VerifyOTPCode()
}
