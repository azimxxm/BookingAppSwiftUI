//
//  SplashScreen.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct SplashScreen: View {
    @State private var isActive = false
    var body: some View {
        if isActive {
            BookingTabView()
        } else {
            ZStack(alignment: .bottom, content: {
                Image("welcomeBG")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                VStack(alignment: .leading, spacing: 32, content: {
                    Text("Welcome to")
                        .font(.urbanistFont(size: 43, weight: .bold))
                        .foregroundStyle(Color.white)
                        .fontWeight(.medium)
                    
                    Text("Bolu")
                        .font(.urbanistFont(size: 96, weight: .bold))
                        .kerning(0.2)
                        .foregroundStyle(Color.brandPrimary)
                    
                    Text("The best hotel bookings in the century to accompany your vacation")
                        .font(.urbanistFont(size: 16, weight: .bold))
                        .kerning(0.2)
                        .foregroundColor(.white)
                })
                .padding()
                .padding(.vertical, 32)
            })
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    withAnimation {
                        self.isActive = true
                    }
                }
            }
            .ignoresSafeArea()
        }
    }
}

#Preview {
    SplashScreen()
}
