//
//  Onboarding.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct Onboarding: View {
    @State var selection: Int
    var body: some View {
        ZStack {
            VStack {
                OnboardingSlider(selection: $selection)
                VStack(spacing: 16, content: {
                    Button(action: {
                        selection += 1
                    }, label: {
                        BookingButton(title: "Next")
                    })
                    Button(action: {
                        selection -= 1
                    }, label: {
                        SkipButton(title: "Skip")
                    })
                })
                .padding(.vertical, 20)
            }
        }
        .ignoresSafeArea()
    }
}

#Preview {
    Onboarding(selection: 1)
}
