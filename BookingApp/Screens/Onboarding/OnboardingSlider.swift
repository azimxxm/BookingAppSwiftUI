//
//  OnboardingSliderStack.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI


struct SliderStack: View {
    var imageName: String
    var title: String
    var bodyMessage: String
    var body: some View {
        VStack (alignment: .center, content: {
            Image(imageName)
                .resizable()
                .scaledToFill()
                .aspectRatio(contentMode: .fit)
                .padding(.bottom, 30)
            Text(title)
                .font(.urbanistFont(size: 32, weight: .bold))
                .kerning(0.2)
                .multilineTextAlignment(.center)
                .foregroundColor(.black)
            Text(bodyMessage)
                .font(.urbanistFont(size: 14, weight: .regular))
                .fixedSize(horizontal: false, vertical: true)
                .lineLimit(10)
                .kerning(0.2)
                .multilineTextAlignment(.center)
            Spacer()
        })
        .ignoresSafeArea()
    }
}

struct OnboardingSlider: View {
    @Binding var selection: Int
    var body: some View {
        TabView(selection: $selection,
                content:  {
            SliderStack(imageName: "onboarding1", title: "Travel safely, comfortably, & easily", bodyMessage: "Lorem ipsum dolor sit amet,  consectetur adipiscing elit,\n sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
                .tag(1)
            
            SliderStack(imageName: "onboarding2", title: "Find the best hotels for your vacation", bodyMessage: "Lorem ipsum dolor sit amet,  consectetur adipiscing elit,\n sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
                .tag(2)
            
            
            SliderStack(imageName: "onboarding3", title: "Let’s discover the world with us", bodyMessage: "Lorem ipsum dolor sit amet,  consectetur adipiscing elit,\n sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
                .tag(3)
        })
        .tabViewStyle(.page)
        .tint(Color.brandPrimary)
        .indexViewStyle(.page(backgroundDisplayMode: .always))
    }
}

#Preview {
    OnboardingSlider(selection: .constant(1))
}
