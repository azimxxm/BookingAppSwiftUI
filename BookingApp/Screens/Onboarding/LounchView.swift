//
//  LounchView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct LounchView: View {
    var body: some View {
        ZStack {
            Color.brandPrimary.ignoresSafeArea()
            Image("logoWhite")
                
        }
    }
}

#Preview {
    LounchView()
}
