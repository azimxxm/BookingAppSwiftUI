//
//  Profile.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 14/10/23.
//

import SwiftUI

struct Profile: View {
    var body: some View {
        NavigationStack {
            Text("Profile")
            .navigationTitle("Profile")
        }
    }
}

#Preview {
    Profile()
}
