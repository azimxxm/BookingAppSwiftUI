//
//  BookingTabView.swift
//  BookingApp
//
//  Created by Azimjon Abdurasulov on 08/10/23.
//

import SwiftUI

struct BookingTabView: View {
    var body: some View {
        TabView {
            Home()
                .tabItem {
                    Label("Home", image: "house.tab")
                }
            Searching()
                .tabItem {
                    Label("Search", image: "search.tab")
                }
            Booking()
                .tabItem {
                    Label("Booking", image: "booking.tab")
                }
            Profile()
                .tabItem {
                    Label("Profile", image: "person.tab")
                }
        }
        .tint(Color.brandPrimary)
    }
}

#Preview {
    BookingTabView()
}
